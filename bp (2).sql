-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 03 Lut 2020, 20:15
-- Wersja serwera: 10.4.8-MariaDB
-- Wersja PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bp`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `administrator`
--

CREATE TABLE `administrator` (
  `Id` int(11) NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Haslo` char(64) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `administrator`
--

INSERT INTO `administrator` (`Id`, `Email`, `Haslo`) VALUES
(1, 'Mail@wp.pl', 'qwerty'),
(2, 'Administrator@onet.pl', 'qwerty123');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hotel`
--

CREATE TABLE `hotel` (
  `Id` int(4) NOT NULL,
  `Nazwa` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Kraj` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Miasto` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Ocena` decimal(11,2) NOT NULL COMMENT 'Średnia wystawionych ocen',
  `Zdjecie` varchar(100) COLLATE utf8mb4_polish_ci NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `hotel`
--

INSERT INTO `hotel` (`Id`, `Nazwa`, `Kraj`, `Miasto`, `Ocena`, `Zdjecie`) VALUES
(1, 'Hesperia', 'Hiszpania', 'Barcelona', '4.50', 'default.jpg'),
(2, 'Grand ', 'Polska ', 'Gdańsk', '2.50', 'default.jpg'),
(3, 'Hilton', 'Polska', 'Warszawa', '4.25', 'default.jpg'),
(4, 'Royal Prague', 'Czechy', 'Praga', '5.00', 'default.jpg'),
(5, 'Colosseum', 'Włochy', 'Rzym', '0.00', 'default.jpg'),
(6, 'Defaul Hotel', 'Polska', 'Białystok', '0.00', 'default.jpg'),
(7, 'Palace Hotel', 'Hiszpania', 'Madryt', '5.00', 'palacehotel.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `Id` int(3) NOT NULL,
  `Login` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Haslo` char(64) COLLATE utf8mb4_polish_ci NOT NULL,
  `Imie` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Nazwisko` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `Tel` int(11) NOT NULL COMMENT 'Numer telefonu',
  `Email` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`Id`, `Login`, `Haslo`, `Imie`, `Nazwisko`, `Tel`, `Email`) VALUES
(1, 'Login', '65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5', 'Jan', 'Kowalski', 123123, 'jkowalski@wp.pl'),
(2, 'Sol', '7137e155f88ba601d0cbcf2137e1de8b57d714a1036829d31fd368c7fad4cb84', 'Adam', 'Nowak', 2147483647, 'mail2231@wp.pl'),
(3, 'NowyLogin', '1555a162ee17853823f14a5e50b9a4372a72634c189a2952428c28f118a372c0', 'Łukasz', 'Nowak', 232132145, 'mail12@gmail.com'),
(4, 'Blendzior', '8588310a98676af6e22563c1559e1ae20f85950792bdcd0c8f334867c54581cd', 'User', 'User', 56123213, 'user@gmail.com');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ocena`
--

CREATE TABLE `ocena` (
  `Id` int(4) NOT NULL,
  `Wartosc` int(1) NOT NULL,
  `Id_hot` int(4) NOT NULL COMMENT 'Id hotelu',
  `Id_kl` int(4) NOT NULL COMMENT 'Id klienta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `ocena`
--

INSERT INTO `ocena` (`Id`, `Wartosc`, `Id_hot`, `Id_kl`) VALUES
(1, 5, 2, 1),
(2, 5, 1, 1),
(3, 3, 4, 1),
(4, 5, 3, 3),
(5, 5, 3, 2),
(6, 3, 3, 2),
(7, 4, 1, 4),
(8, 5, 7, 4),
(9, 0, 2, 4),
(11, 4, 3, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wiadomosc`
--

CREATE TABLE `wiadomosc` (
  `Id` int(4) NOT NULL,
  `pytanie` text COLLATE utf8mb4_polish_ci NOT NULL COMMENT 'pytanie od klienta',
  `odpowiedz` text COLLATE utf8mb4_polish_ci DEFAULT NULL COMMENT 'odpowiedz od administratora',
  `Id_kl` int(4) NOT NULL,
  `Id_wyc` int(4) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT 0 COMMENT '0- nieprzeczytane, 1 - przeczytane'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `wiadomosc`
--

INSERT INTO `wiadomosc` (`Id`, `pytanie`, `odpowiedz`, `Id_kl`, `Id_wyc`, `Status`) VALUES
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien ligula, rutrum id blandit at, sodales vitae diam. Sed luctus elementum nulla id bibendum. Quisque tristique in tortor a tincidunt. Nulla et consectetur erat. Curabitur vel vestibulum enim. Maecenas finibus ligula dui, eu blandit nulla fermentum sit amet. Suspendisse tristique ligula eget lectus hendrerit, eget efficitur ipsum posuere. Duis pulvinar ex vel aliquet facilisis. Sed luctus odio quam, in mattis arcu facilisis ut.\r\n\r\nUt in congue nunc. Donec interdum purus dolor, nec dictum arcu vehicula eu. Praesent vel velit sit amet magna posuere congue. Integer dui lectus, pulvinar vel eleifend ac, pharetra ut nibh. Vivamus condimentum, ante non bibendum placerat, erat purus luctus orci, quis sodales nibh lectus condimentum enim. Aliquam ut felis et velit facilisis mollis. Ut ac ex at quam semper volutpat.\r\n\r\nVestibulum sem eros, iaculis vitae lacus et, ullamcorper convallis quam. Nunc tempor ipsum et ipsum vestibulum, vitae sagittis nisi blandit. Sed egestas efficitur nunc, hendrerit varius lorem tempus vulputate. Pellentesque commodo magna sit amet urna imperdiet lobortis. Sed egestas consequat metus, id commodo est euismod vitae. Phasellus magna ante, varius sed enim quis, luctus placerat lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec id augue vitae tortor dictum fringilla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque consequat egestas ultricies. Proin efficitur mi vel odio hendrerit, et interdum lectus luctus. Morbi ac bibendum lectus.', 'Odp', 4, 3, 1),
(4, 'Pytanie nr 1\r\n                    \r\n                    ', 'odpowiedz\r\n                    ', 2, 2, 1),
(5, 'Pytanie nr 1\r\n    ', 'Odpowiedz                   ', 4, 3, 1),
(6, 'Pytanie nr 2\r\n                    ', '\r\n                    ', 4, 2, 1),
(7, 'Pytanie nr 1\r\n                    ', 'Odpowiadam\r\n                    ', 4, 5, 1),
(8, 'Pytanie \r\n                    ', 'Odpowiadam\r\n                    ', 4, 10, 0),
(9, 'Pytanie \r\n                    ', NULL, 4, 2, 0),
(10, 'Pytanie \r\n                    ', NULL, 4, 3, 0),
(11, 'Pytanie \r\n                    ', NULL, 4, 9, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wycieczka`
--

CREATE TABLE `wycieczka` (
  `Id` int(4) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  `Data1` date NOT NULL COMMENT 'Data wyjazdu',
  `Data2` date NOT NULL COMMENT 'Data powrotu',
  `Wyzywienie` enum('Wlasny zakres','Sniadanie','Sniadania i kolacje','All inclusive') COLLATE utf8mb4_polish_ci NOT NULL,
  `Miejsca` int(2) NOT NULL DEFAULT 10 COMMENT 'Wolne miejsca',
  `Id_hot` int(4) NOT NULL COMMENT 'Id hotelu',
  `Id_adm` int(4) NOT NULL COMMENT 'Id administratora'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `wycieczka`
--

INSERT INTO `wycieczka` (`Id`, `Cena`, `Data1`, `Data2`, `Wyzywienie`, `Miejsca`, `Id_hot`, `Id_adm`) VALUES
(2, '1999.99', '2019-11-10', '2019-11-16', 'All inclusive', 3, 2, 1),
(3, '2999.99', '2019-11-06', '2019-12-13', 'Wlasny zakres', 13, 1, 1),
(4, '1500.99', '2019-11-24', '2019-11-30', 'Sniadania i kolacje', 7, 2, 1),
(5, '2000.00', '2019-11-20', '2019-11-27', 'Wlasny zakres', 3, 3, 1),
(6, '2499.99', '2019-11-27', '2019-11-30', 'Sniadanie', 7, 3, 1),
(7, '999.99', '2019-12-01', '2019-12-07', 'Sniadania i kolacje', 13, 4, 1),
(9, '999.99', '2019-12-16', '2019-12-22', 'Sniadania i kolacje', 10, 2, 1),
(10, '999.99', '2020-01-01', '2020-01-10', 'Sniadania i kolacje', 4, 1, 1),
(11, '999.99', '2020-01-20', '2020-01-26', 'All inclusive', 10, 4, 1),
(12, '2888.00', '2020-01-13', '2019-12-29', 'Sniadania i kolacje', 8, 5, 1),
(13, '4999.00', '2020-02-10', '2020-02-16', 'All inclusive', 10, 7, 1),
(14, '4000.00', '2020-03-16', '2020-03-22', 'Sniadania i kolacje', 10, 4, 2),
(17, '2323.00', '2020-03-02', '2020-03-08', 'Sniadania i kolacje', 10, 6, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia`
--

CREATE TABLE `zamowienia` (
  `Id` int(4) NOT NULL,
  `Il_osob` int(2) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  `Id_kl` int(4) NOT NULL,
  `Id_wyc` int(4) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT 0 COMMENT '0 - oczekuje na akceptację 1- zaakceptowana oferta, 2 oferta odrzucona przez administratora, 3 oferta odrzucona przez klienta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `zamowienia`
--

INSERT INTO `zamowienia` (`Id`, `Il_osob`, `Cena`, `Id_kl`, `Id_wyc`, `Status`) VALUES
(1, 4, '8000.00', 4, 2, 2),
(2, 3, '3000.00', 4, 10, 1),
(3, 5, '7505.00', 4, 4, 2),
(4, 5, '7504.95', 4, 4, 3),
(5, 3, '7499.97', 4, 6, 1),
(6, 2, '4999.98', 4, 6, 2),
(7, 3, '5999.97', 4, 2, 3),
(8, 2, '3999.98', 4, 2, 1),
(9, 5, '14999.95', 4, 3, 1),
(10, 4, '11999.96', 1, 3, 1),
(11, 4, '8000.00', 2, 5, 1),
(12, 3, '2999.97', 4, 10, 3),
(13, 3, '8999.97', 4, 3, 0),
(14, 2, '3999.98', 4, 2, 0),
(15, 3, '6000.00', 4, 5, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indeksy dla tabeli `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Login` (`Login`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indeksy dla tabeli `ocena`
--
ALTER TABLE `ocena`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_hot` (`Id_hot`),
  ADD KEY `Id_kl` (`Id_kl`);

--
-- Indeksy dla tabeli `wiadomosc`
--
ALTER TABLE `wiadomosc`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_kl` (`Id_kl`),
  ADD KEY `Id_adm,` (`Id_wyc`);

--
-- Indeksy dla tabeli `wycieczka`
--
ALTER TABLE `wycieczka`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_hot` (`Id_hot`),
  ADD KEY `Id_adm` (`Id_adm`);

--
-- Indeksy dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_kl` (`Id_kl`),
  ADD KEY `Id_wyc` (`Id_wyc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `administrator`
--
ALTER TABLE `administrator`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `hotel`
--
ALTER TABLE `hotel`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `Id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ocena`
--
ALTER TABLE `ocena`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `wiadomosc`
--
ALTER TABLE `wiadomosc`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `wycieczka`
--
ALTER TABLE `wycieczka`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `ocena`
--
ALTER TABLE `ocena`
  ADD CONSTRAINT `ocena_ibfk_1` FOREIGN KEY (`Id_hot`) REFERENCES `hotel` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ocena_ibfk_2` FOREIGN KEY (`Id_kl`) REFERENCES `klient` (`Id`);

--
-- Ograniczenia dla tabeli `wiadomosc`
--
ALTER TABLE `wiadomosc`
  ADD CONSTRAINT `wiadomosc_ibfk_1` FOREIGN KEY (`Id_kl`) REFERENCES `klient` (`Id`),
  ADD CONSTRAINT `wiadomosc_ibfk_2` FOREIGN KEY (`Id_wyc`) REFERENCES `wycieczka` (`Id`);

--
-- Ograniczenia dla tabeli `wycieczka`
--
ALTER TABLE `wycieczka`
  ADD CONSTRAINT `Name` FOREIGN KEY (`Id_hot`) REFERENCES `hotel` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `wycieczka_ibfk_1` FOREIGN KEY (`Id_adm`) REFERENCES `administrator` (`Id`);

--
-- Ograniczenia dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  ADD CONSTRAINT `zamowienia_ibfk_1` FOREIGN KEY (`Id_kl`) REFERENCES `klient` (`Id`),
  ADD CONSTRAINT `zamowienia_ibfk_2` FOREIGN KEY (`Id_wyc`) REFERENCES `wycieczka` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
