<?php
    session_start();
?>

<!DOCTYPE HTML>

<HTML>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>Strona główna</title>
        
        <script>
            function szukanie() {
  var x = document.getElementById("szukanie");
  if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
</script>
    </head>
    <body>
        <header>
            <?php
                if(isset($_SESSION['OK']))
                {
                    echo $_SESSION['OK'];
                    unset($_SESSION['OK']);
                }
                if(isset($_SESSION['logok']))
                {
                    echo $_SESSION['logok'];
                    unset($_SESSION['logok']);
                }
                if(isset($_SESSION['logfail']))
                {
                    echo $_SESSION['logfail'];
                    unset($_SESSION['logfail']);
                }
                
            ?>
            
        </header>
        
        <nav>
            <ul>
            <li><a href="index.php">Strona główna</a></li>
            <?php
                if(!isset($_SESSION['zalogowany']))
                {
                    echo  '<li><a href="rejestracja.php">Zarejestruj się</a></li>';
                    echo '<li><a href="logowanie.php">Logowanie</a></li>';
                }
                if(isset($_SESSION['admin']))
                {
                    
                    $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                     $ill = "select *, zamowienia.id as z_id, zamowienia.cena as z_cena, wycieczka.Id as wyc_id from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id where wycieczka.Id_adm=".$_SESSION['id']. ' and Status=0';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill2=$dbh->prepare($ill);
                    $ill2->execute();
                    $res = $ill2->rowCount();
                    echo '<li><a href="panel.php">Panel administracyjny</a></li>';
                    echo '<li><a href="zarzadzaniezamowieniami.php">Zarządzanie zamówieniami';
                        if($res==0)
                        {
                            echo ' ('.$res.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res.')</span>';
                        }
                    echo'</a></li>';
                }
                
                if(isset($_SESSION['admin']))
                {
                    
                     $ill3 = "select * from wiadomosc inner join wycieczka on wiadomosc.Id_wyc=wycieczka.Id WHERE wycieczka.Id_adm=".$_SESSION['id'].' and wiadomosc.odpowiedz IS NULL';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill4=$dbh->prepare($ill3);
                    $ill4->execute();
                    $res4 = $ill4->rowCount();
                    echo '<li><a href="zarzadzaniewiadomosci.php">Wiadomości';
                        if($res4==0)
                        {
                            echo ' ('.$res4.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res4.')</span>';
                        }
                    echo'</a></li>';
                }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                        echo '<li>  <a href="twojezamowienia.php">Twoje Zamowienia</a></li>';   
                    }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                     $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                       $ill5 = "select * from wiadomosc where Id_kl=".$_SESSION['id'].' and Status=0 and odpowiedz is NOT NULL';
                       $ill6 = $dbh->prepare($ill5);
                       $ill6->execute();
                       $res5=$ill6->rowCount();
                        echo '<li>  <a href="twojewiadomosci.php">Wiadomości';
                         if($res5==0)
                        {
                            echo ' ('.$res5.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res5.')</span>';
                        }
                        
                        echo '</a></li>';
                        
                    }
                if(isset($_SESSION['zalogowany']))
                {
                    echo '<li class="lii"><a href="wyloguj.php">Zalogowany jako: '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' [Wyloguj]</a></li>';
                    if(!isset($_SESSION['uprawnienia'])) 
                    {
                           // echo '<a href="historia.php">Twoje wypożyczenia</a>';
                            //echo '<a href="doladowanie.php">Stan konta: '.$_SESSION['stan_konta'].'</a>';
                            //echo '<a href="doladowanie.php">Status: '.$_SESSION['stat'].'</a>';
                    }
                   
                }
            
            ?>
            </ul>
        
        </nav>
        
        <aside>
               <?php
                if(isset($_SESSION['ocenianie']))
                {   
                    echo '<p>UWAGA!</p>';
                    echo $_SESSION['ocenianie'];
                    unset($_SESSION['ocenianie']);
                }
             if(isset($_SESSION['pytanie']))
                {   
                    echo '<p>UWAGA!</p>';
                    echo $_SESSION['pytanie'];
                    unset($_SESSION['pytanie']);
                }
            ?>
            <button onclick="szukanie()" class="szukaj">Wyszukiwanie ofert</button>
            <div id="szukanie">
            <h1>Wyszukiwanie wycieczek:</h1>
            <form action="szukaj.php" method="GET">
                <p>Kraj</p>
                <input type="text" name="kraj">
                <p>Miejscowość</p>
                <input type="text" name="miasto">
                <p>Hotel</p>
                <input type="text" name="hotel">
                <p>Wyjazd od:</p>
                <input type="date" name="wyjazd"  value="<?php //echo date('Y-m-d'); ?>" >
                <p>Powrót do:</p>
                <input type="date" name="powrot" >
                <p>Wyżywienie</p>
                <select name="wyzywienie">
                    <option value="" >Dowolne</option>
                    <option value="Wlasny zakres">We własnym zakresie</option>
                    <option value="Sniadanie">Śniadania</option>
                    <option value="Sniadania i kolacje">Śniadania i kolacje</option>
                    <option value="All inclusive">All inclusive</option>
                </select>
                <p>Cena:</p>
                Od:<br>
                <select name="cena1">
                    <?php
                    for($x=0; $x <=20; $x++)
                    {
                        echo '<option>'.($x*500).'</option>';
                    }
                    ?>
                </select>
                <br>Do:<br>
                <select  name="cena2">
                    <?php
                    for($x=0; $x <=20; $x++)
                    {
                        echo '<option>'.($x*500).'</option>';
                    }
                    ?>
                </select>

           
            <p>Sortuj według:</p>
            <select name="sort">
                <option value="cena">Cena</option>
                <option value="data1">Data wyjazdu</option>
                <option value="data2">Data powrotu</option>
            </select><p></p>
              <input type="radio" name="sorttype" value="mal" checked="checked"> Malejąco<br>
              <input type="radio" name="sorttype" value="ros"> Rosnąco<br>
            
                <br><br>
                <input type="submit" value="Szukaj"/>
                <br><br>
                 </form>
        </div>
        </aside>
        
        <article>
                <div id="main">
                <?php
                    
                    $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                    $results_on_page = 6;
                    $results = $dbh->prepare("select * from wycieczka where miejsca>0");
                    $results->execute();
                    $results_num = $results->rowCount();
                    $number_of_pages = ceil($results_num/$results_on_page);
                    if(!isset($_GET['page']))
                    {
                        $page=1;
                        $zm_pom=1;
                    }
                    else
                    {
                        $page = $_GET['page'];
                        $zm_pom=$_GET['page'];
                    }
                    $first_page = ($page-1)*$results_on_page;
                    $qr = "select *, w.Id as w_id, h.Id as h_id, w.Id_adm as w_id_adm from wycieczka w inner join hotel h on w.id_hot=h.id where miejsca>0 LIMIT ". $first_page . ','. $results_on_page;
                    foreach($dbh -> query($qr) as $zmienna)
                     {
                        $datetime1 = date_create($zmienna['Data1']); 
                        $datetime2 = date_create($zmienna['Data2']); 
  
                        $interval = date_diff($datetime1, $datetime2); 

                        echo '<div class="oferta">';
                        echo ' <img src="'.$zmienna['Zdjecie'].'" alt="'.$zmienna['Nazwa'].'"><p></p>';
                       // echo $zmienna['Id'].'  '.$pom.'<br>';
                        echo '<h2>'.$zmienna['Nazwa'].'</h2>  <h3>'.$zmienna['Kraj'].' / '.$zmienna['Miasto'].'</h3>';
                        echo '<ul>
                                <li>'.$zmienna['Data1'].' - '.$zmienna['Data2'].'</li>
                                <li>'.$zmienna['Cena'].' zł</li>
                                <li>'.$zmienna['Wyzywienie'].' </li>
                                <li>'.$interval->format('%a dni').' </li>
                                <li>Wolne miejsca: '.$zmienna['Miejsca'].' </li>
                                <li>Ocena klientów: '.$zmienna['Ocena'].' </li>
                             </ul>';
                        
                        
                        echo '<form action="wycieczka.php" method="get">
                                <input type="hidden" name="id" value="'.$zmienna['w_id'].'">
                                <input type="submit" value="Zobacz ofertę">
                             
                             </form>';
                           echo '<form action="ocenianie.php" method="post">
                                <input type="hidden" name="id" value="'.$zmienna['h_id'].'">
                                <input type="submit" value="Oceń hotel">
                             
                             </form>';
                        
                        echo '<form action="zamowienie.php" method="post">
                                <input type="hidden" name="id" value="'.$zmienna['w_id'].'">
                                <input type="submit" value="Zamów wycieczkę">
                             
                             </form>';
                        echo '<form action="zadajpytanie.php" method="post">
                                <input type="hidden" name="w_id" value="'.$zmienna['w_id'].'">
                                <input type="hidden" name="id_adm" value="'.$zmienna['w_id_adm'].'">
                                <input type="submit" value="Zadaj pytanie">
                             
                             </form>';
                        echo '</div>';
                        
                       
                    }
                ?>
                    </div>
            <div class="pagination">
<!--                <a href="#">&laquo;</a>-->
            <?php
                if($zm_pom==1)
                {
                    echo '<a href="#">&laquo;</a>';
                }
                else
                {
                    echo '<a href="index.php?page='.($zm_pom-1).'">&laquo;</a>';
                }
               // echo '<p></p>';
                     for($page=1;$page<=$number_of_pages;$page++)
                        {
                            if($zm_pom==$page)
                            {
                                 echo '<a href="index.php?page='.$page.'" class="active"> ' .$page . '  </a>';
                            }
                            else
                            {
                                echo '<a href="index.php?page='.$page.'"> ' .$page . '  </a>';
                            }                           
                        }
                if($zm_pom==$number_of_pages)
                {
                    echo '<a href="#">&raquo;</a>';
                }
                else
                {
                    echo '<a href="index.php?page='.($zm_pom+1).'">&raquo;</a>';
                }
                ?>
                
            </div>
        </article>
        <footer>
        
        </footer>
    
    </body>

</HTML>