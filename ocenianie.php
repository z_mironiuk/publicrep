<?php
    session_start();
 if(!isset($_SESSION['zalogowany']))
    {
        header('location: logowanie.php');
    }
?>

<!DOCTYPE HTML>

<HTML>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>Strona główna</title>
    </head>
    <body>
        <header>
        
        </header>
        <nav>
            <ul>
            <li><a href="index.php">Strona główna</a></li>
            <?php
                if(!isset($_SESSION['zalogowany']))
                {
                    echo  '<li><a href="rejestracja.php">Zarejestruj się</a></li>';
                    echo '<li><a href="logowanie.php">Logowanie</a></li>';
                }
                if(isset($_SESSION['admin']))
                {
                    
                    $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                     $ill = "select *, zamowienia.id as z_id, zamowienia.cena as z_cena, wycieczka.Id as wyc_id from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id where wycieczka.Id_adm=".$_SESSION['id']. ' and Status=0';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill2=$dbh->prepare($ill);
                    $ill2->execute();
                    $res = $ill2->rowCount();
                    echo '<li><a href="panel.php">Panel administracyjny</a></li>';
                    echo '<li><a href="zarzadzaniezamowieniami.php">Zarządzanie zamówieniami';
                        if($res==0)
                        {
                            echo ' ('.$res.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res.')</span>';
                        }
                    echo'</a></li>';
                }
                
                if(isset($_SESSION['admin']))
                {
                    
                     $ill3 = "select * from wiadomosc inner join wycieczka on wiadomosc.Id_wyc=wycieczka.Id WHERE wycieczka.Id_adm=".$_SESSION['id'].' and wiadomosc.odpowiedz IS NULL';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill4=$dbh->prepare($ill3);
                    $ill4->execute();
                    $res4 = $ill4->rowCount();
                    echo '<li><a href="zarzadzaniewiadomosci.php">Wiadomości';
                        if($res4==0)
                        {
                            echo ' ('.$res4.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res4.')</span>';
                        }
                    echo'</a></li>';
                }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                        echo '<li>  <a href="twojezamowienia.php">Twoje Zamowienia</a></li>';   
                    }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                     $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                       $ill5 = "select * from wiadomosc where Id_kl=".$_SESSION['id'].' and Status=0 and odpowiedz is NOT NULL';
                       $ill6 = $dbh->prepare($ill5);
                       $ill6->execute();
                       $res5=$ill6->rowCount();
                        echo '<li>  <a href="twojewiadomosci.php">Wiadomości';
                         if($res5==0)
                        {
                            echo ' ('.$res5.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res5.')</span>';
                        }
                        
                        echo '</a></li>';
                        
                    }
                if(isset($_SESSION['zalogowany']))
                {
                    echo '<li class="lii"><a href="wyloguj.php">Zalogowany jako: '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' [Wyloguj]</a></li>';
                    if(!isset($_SESSION['uprawnienia'])) 
                    {
                           // echo '<a href="historia.php">Twoje wypożyczenia</a>';
                            //echo '<a href="doladowanie.php">Stan konta: '.$_SESSION['stan_konta'].'</a>';
                            //echo '<a href="doladowanie.php">Status: '.$_SESSION['stat'].'</a>';
                    }
                   
                }
            
            ?>
            </ul>
        
        </nav>
        <article class="wyc">
            <br><br>
            <h2>Ocenianie hotelu:</h2>
            <?php
                $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                $id = $_POST['id'];
                $qr = "Select * from hotel where id=:id";
                $rs=$dbh->prepare($qr);
                $rs->execute([':id' => $id]);
                $info=$rs->fetch(PDO::FETCH_ASSOC);
                echo '<h3>'.$info['Nazwa'].'</h3> ';
                echo '<h4>'.$info['Kraj'].' / '.$info['Miasto'];
            ?>
            <form action="ocen.php" method="post">
                <p>Podaj ocenę:</p>
                <input type="number" name="ocena" max="5" min="1" required>
                <input type="hidden" name="hotel" value="<?php echo $_POST['id']; ?>">
                <input type="submit" value="Oceń!">
            </form>
           
            <p><a href="index.php">Powrót do strony głównej</a></p>
        </article>
        
        <footer>
        
        </footer>
        
        
        
        
        
        
                
    </body>
</HTML>