<?php
    session_start();
?>

<!DOCTYPE HTML>

<HTML>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>Strona główna</title>
    </head>
    <body>
        <header>
        
        </header>
         <nav>
            <ul>
            <li><a href="index.php">Strona główna</a></li>
            <?php
                if(!isset($_SESSION['zalogowany']))
                {
                    echo  '<li><a href="rejestracja.php">Zarejestruj się</a></li>';
                    echo '<li><a href="logowanie.php">Logowanie</a></li>';
                }
                if(isset($_SESSION['admin']))
                {
                    
                    $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                     $ill = "select *, zamowienia.id as z_id, zamowienia.cena as z_cena, wycieczka.Id as wyc_id from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id where wycieczka.Id_adm=".$_SESSION['id']. ' and Status=0';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill2=$dbh->prepare($ill);
                    $ill2->execute();
                    $res = $ill2->rowCount();
                    echo '<li><a href="panel.php">Panel administracyjny</a></li>';
                    echo '<li><a href="zarzadzaniezamowieniami.php">Zarządzanie zamówieniami';
                        if($res==0)
                        {
                            echo ' ('.$res.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res.')</span>';
                        }
                    echo'</a></li>';
                }
                
                if(isset($_SESSION['admin']))
                {
                    
                     $ill3 = "select * from wiadomosc inner join wycieczka on wiadomosc.Id_wyc=wycieczka.Id WHERE wycieczka.Id_adm=".$_SESSION['id'].' and wiadomosc.odpowiedz IS NULL';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill4=$dbh->prepare($ill3);
                    $ill4->execute();
                    $res4 = $ill4->rowCount();
                    echo '<li><a href="zarzadzaniewiadomosci.php">Wiadomości';
                        if($res4==0)
                        {
                            echo ' ('.$res4.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res4.')</span>';
                        }
                    echo'</a></li>';
                }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                        echo '<li>  <a href="twojezamowienia.php">Twoje Zamowienia</a></li>';   
                    }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                     $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                       $ill5 = "select * from wiadomosc where Id_kl=".$_SESSION['id'].' and Status=0 and odpowiedz is NOT NULL';
                       $ill6 = $dbh->prepare($ill5);
                       $ill6->execute();
                       $res5=$ill6->rowCount();
                        echo '<li>  <a href="twojewiadomosci.php">Wiadomości';
                         if($res5==0)
                        {
                            echo ' ('.$res5.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res5.')</span>';
                        }
                        
                        echo '</a></li>';
                        
                    }
                if(isset($_SESSION['zalogowany']))
                {
                    echo '<li class="lii"><a href="wyloguj.php">Zalogowany jako: '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' [Wyloguj]</a></li>';
                    if(!isset($_SESSION['uprawnienia'])) 
                    {
                           // echo '<a href="historia.php">Twoje wypożyczenia</a>';
                            //echo '<a href="doladowanie.php">Stan konta: '.$_SESSION['stan_konta'].'</a>';
                            //echo '<a href="doladowanie.php">Status: '.$_SESSION['stat'].'</a>';
                    }
                   
                }
            
            ?>
            </ul>
        
        </nav>
        
        <article>
            <h1>Twoje zamówienia</h1>
            <?php
                $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                $qrr = "select *, zamowienia.id as z_id, zamowienia.cena as z_cena, wycieczka.Id as wyc_id from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id inner join hotel on wycieczka.Id_hot=hotel.Id where zamowienia.Id_kl=".$_SESSION['id']. ' and Status!=2';
            
                $rs=$dbh->prepare("select * from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id where zamowienia.Id_kl=:id");
                $rs->execute([':id'=>$_SESSION['id']]);
                $info=$rs->fetch(PDO::FETCH_ASSOC);
                //echo $info['Id'];
                
               // $rs2=$dbh->prepare("select * from hotel where id=:id");
               // $rs2->execute([':id'=>$info['Id_hot']]);
               // $info2=$rs2->fetch(PDO::FETCH_NUM);
                echo '<h2>Oferty oczekujące:</h2>';
                foreach($dbh->query($qrr) as $zmienna)
                {
                    if($zmienna['Status']==0)
                    {
                    echo '<div class="oferta">';
                    echo '<h3>Zamowienie nr '.$zmienna['z_id'].'</h3><br>';
                    echo 'Ilość osób: '.$zmienna['Il_osob'].'<br>';
                    echo 'Cena: '.$zmienna['z_cena'].'<br>';
                    echo 'Data wyjazdu: '.$zmienna['Data1'].'<br>';
                    echo 'Data powrotu: '.$zmienna['Data2'].'<br>';
                    echo 'Wyzywienie: '.$zmienna['Wyzywienie'].'<br>';
                    echo 'Nazwa hotelu: '.$zmienna['Nazwa'].'<br>';
                    echo 'Kraj: '.$zmienna['Kraj'].'<br>';
                    echo 'Miasto: '.$zmienna['Miasto'].'<br>';
                    echo 'Status: '.$zmienna['Status'].'<br>';
                    echo '<p></p>';
                      echo '<form action="odrzuczamowienieklient.php" method="post">
                                <input type="hidden" name="id" value="'.$zmienna['z_id'].'">
                                 <input type="hidden" name="wyc_id" value="'.$zmienna['wyc_id'].'">
                                <input type="hidden" name="osoby" value="'.$zmienna['Il_osob'].'">
                                <input type="submit" value="Odrzuć ofertę">
                            </form>';
                    echo '</div>';
                    }
                    
                }
            
                 echo '<h2>Oferty zaakceptowane:</h2>';
            
              foreach($dbh->query($qrr) as $zmienna)
                {
                    if($zmienna['Status']==1)
                    {
                    echo '<div class="oferta">';
                    echo '<h3>Zamowienie nr '.$zmienna['z_id'].'</h3><br>';
                    echo 'Ilość osób: '.$zmienna['Il_osob'].'<br>';
                    echo 'Cena: '.$zmienna['z_cena'].'<br>';
                    echo 'Data wyjazdu: '.$zmienna['Data1'].'<br>';
                    echo 'Data powrotu: '.$zmienna['Data2'].'<br>';
                    echo 'Wyzywienie: '.$zmienna['Wyzywienie'].'<br>';
                    echo 'Nazwa hotelu: '.$zmienna['Nazwa'].'<br>';
                    echo 'Kraj: '.$zmienna['Kraj'].'<br>';
                    echo 'Miasto: '.$zmienna['Miasto'].'<br>';
                    echo 'Status: '.$zmienna['Status'].'<br>';
                    echo '<p></p>';
        
                    echo '</div>';
                    }
                }
               
//            
//               echo '<form action="ocen.php" method="post">
//                                <input type="hidden" name="id" value="'.$info['h_id'].'">
//                                <input type="submit" value="Oceń hotel">
//                             
//                             </form>';
//                        
//                        echo '<form action="zamowienie.php" method="post">
//                                <input type="hidden" name="id" value="'.$info['w_id'].'">
//                                <input type="submit" value="Zamów wycieczkę">
//                             
//                             </form>';
            ?>
            <p><a href="index.php">Powrót do strony głównej</a></p>
        </article>
        
        <footer>
        
        </footer>
        
        
        
        
        
        
                
    </body>
</HTML>