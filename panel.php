<?php
    session_start();
?>

<!DOCTYPE HTML>

<HTML>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>Panel administracyjny</title>
    </head>
    <body>
        <header>
        
        </header>
       <nav>
            <ul>
            <li><a href="index.php">Strona główna</a></li>
            <?php
                if(!isset($_SESSION['zalogowany']))
                {
                    echo  '<li><a href="rejestracja.php">Zarejestruj się</a></li>';
                    echo '<li><a href="logowanie.php">Logowanie</a></li>';
                }
                if(isset($_SESSION['admin']))
                {
                    
                    $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                     $ill = "select *, zamowienia.id as z_id, zamowienia.cena as z_cena, wycieczka.Id as wyc_id from zamowienia inner join wycieczka on zamowienia.Id_wyc=wycieczka.Id where wycieczka.Id_adm=".$_SESSION['id']. ' and Status=0';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill2=$dbh->prepare($ill);
                    $ill2->execute();
                    $res = $ill2->rowCount();
                    echo '<li><a href="panel.php">Panel administracyjny</a></li>';
                    echo '<li><a href="zarzadzaniezamowieniami.php">Zarządzanie zamówieniami';
                        if($res==0)
                        {
                            echo ' ('.$res.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res.')</span>';
                        }
                    echo'</a></li>';
                }
                
                if(isset($_SESSION['admin']))
                {
                    
                     $ill3 = "select * from wiadomosc inner join wycieczka on wiadomosc.Id_wyc=wycieczka.Id WHERE wycieczka.Id_adm=".$_SESSION['id'].' and wiadomosc.odpowiedz IS NULL';
                   // $ill = "select *, zamowienia.Id as z_id, wycieczka.Id as w_id from zamowienia inner join wycieczka on z_id=w_id where zamowienia.Status=0 and wycieczka.Id_adm=".$_SESSION['id']."";
                    $ill4=$dbh->prepare($ill3);
                    $ill4->execute();
                    $res4 = $ill4->rowCount();
                    echo '<li><a href="zarzadzaniewiadomosci.php">Wiadomości';
                        if($res4==0)
                        {
                            echo ' ('.$res4.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res4.')</span>';
                        }
                    echo'</a></li>';
                }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                        echo '<li>  <a href="twojezamowienia.php">Twoje Zamowienia</a></li>';   
                    }
                
                 if(!isset($_SESSION['admin']) && isset($_SESSION['zalogowany']))
                    {
                     $dbh = new PDO('mysql:host=localhost;dbname=BP','root','');
                       $ill5 = "select * from wiadomosc where Id_kl=".$_SESSION['id'].' and Status=0 and odpowiedz is NOT NULL';
                       $ill6 = $dbh->prepare($ill5);
                       $ill6->execute();
                       $res5=$ill6->rowCount();
                        echo '<li>  <a href="twojewiadomosci.php">Wiadomości';
                         if($res5==0)
                        {
                            echo ' ('.$res5.')';
                        }
                        else 
                        {
                            echo '<span style="color:red"> ('.$res5.')</span>';
                        }
                        
                        echo '</a></li>';
                        
                    }
                if(isset($_SESSION['zalogowany']))
                {
                    echo '<li class="lii"><a href="wyloguj.php">Zalogowany jako: '.$_SESSION['imie'].' '.$_SESSION['nazwisko'].' [Wyloguj]</a></li>';
                    if(!isset($_SESSION['uprawnienia'])) 
                    {
                           // echo '<a href="historia.php">Twoje wypożyczenia</a>';
                            //echo '<a href="doladowanie.php">Stan konta: '.$_SESSION['stan_konta'].'</a>';
                            //echo '<a href="doladowanie.php">Status: '.$_SESSION['stat'].'</a>';
                    }
                   
                }
            
            ?>
            </ul>
        
        </nav>
        <article class="wyc">
            <div id="rejestracja">
            <h2>Dodaj wycieczkę:</h2>
            <form action="dodajWycieczke.php" method="post">
                 <p>Wybierz hotel</p>
                <select name="hotel" required>
                    <?php
                         $dbh = new PDO ('mysql:host=localhost;dbname=BP','root','');
                        $hotel = "select * from hotel";
                        foreach($dbh -> query($hotel) as $zmienna)
                        {
                            echo '<option value ='.$zmienna['Id'].'>'.$zmienna['Id'].' / '.$zmienna['Nazwa'].' / '.$zmienna['Kraj'].' / '. $zmienna['Miasto'].'</option>';
                        }
                    ?>
                </select>
                <p>Data wyjazdu</p>
                <input type="date" name="wyjazd" required >
                <p>Data powrotu</p>
                <input type="date" name="powrot" required >
                <p>Wyżywienie</p>
                <select name="wyzywienie" required>
                    <option value="Wlasny zakres">We własnym zakresie</option>
                    <option value="Sniadanie">Śniadania</option>
                    <option value="Sniadania i kolacje">Śniadania i kolacje</option>
                    <option value="All inclusive">All inclusive</option>
                </select>
                <p>Cena:</p>
                <input type="number" name="cena" required><p></p>
                
                <input type="submit" value="Dodaj nową wycieczkę">
            
            </form>
                <?php
                if(isset($_SESSION['wycieczka']))
                {
                    echo $_SESSION['wycieczka'];
                    unset($_SESSION['wycieczka']);
                }
            ?>
            <h2>Usuń wycieczkę:</h2>
            <form action="usunWycieczke.php" method="post">
                <p>Wybierz wycieczkę:</p>
                <select name="wycieczka" required>
                    <?php
                         $dbh = new PDO ('mysql:host=localhost;dbname=BP','root','');
                        $hotel =  "select *, w.Id as w_id, h.Id as h_id from wycieczka w inner join hotel h on w.id_hot=h.id";
                        foreach($dbh -> query($hotel) as $zmienna)
                        {
                             echo '<option value ='.$zmienna['w_id'].'>'.$zmienna['w_id'].' / '.$zmienna['Nazwa'].' / '.$zmienna['Data1'].' /'.$zmienna['Data2'].' / '.$zmienna['Cena'].'zł</option>';
                        }
                    ?>
                </select><p></p>
                
                <input type="submit" value="Usuń wycieczkę">
                
            </form>
                <?php
                if(isset($_SESSION['wycieczkausun']))
                {
                    echo $_SESSION['wycieczkausun'];
                    unset($_SESSION['wycieczkausun']);
                }
            ?>
            <h2>Modyfikuj wycieczkę:</h2>
            <form action="modyfikacjaWycieczki.php" method="post">
                <p>Wybierz wycieczkę:</p>
                <select name="wycieczka" required>
                    <?php
                         $dbh = new PDO ('mysql:host=localhost;dbname=BP','root','');
                        $hotel =  "select *, w.Id as w_id, h.Id as h_id from wycieczka w inner join hotel h on w.id_hot=h.id";
                        foreach($dbh -> query($hotel) as $zmienna)
                        {
                            echo '<option value ='.$zmienna['w_id'].'>'.$zmienna['w_id'].' / '.$zmienna['Nazwa'].' / '.$zmienna['Data1'].' /'.$zmienna['Data2'].' / '.$zmienna['Cena'].'zł</option>';
                        }
                    ?>
                </select><p></p>
                
                <input type="submit" value="Modyfikuj wycieczkę">
                
            </form>
                
                 <?php
                if(isset($_SESSION['wycieczkamodyfikacja']))
                {
                    echo $_SESSION['wycieczkamodyfikacja'];
                    unset($_SESSION['wycieczkamodyfikacja']);
                }
            ?>
            
            <h2>Dodaj hotel:</h2>
            <form action="dodajHotel.php" method="post" enctype="multipart/form-data">
                <p>Nazwa</p>
                <input type="text" name="nazwa" required >
                <p>Kraj</p>
                <input type="text" name="kraj" required >
                <p>Miasto</p>
                <input type="text" name="miasto" required > <p></p>
                Zdjęcie:<p></p>
                <input type="file" name="fileToUpload" id="fileToUpload"><p></p>
                
                <input type="submit" value="Dodaj nowy hotel">
            
            </form>
                <?php
                if(isset($_SESSION['dodajhotel']))
                {
                    echo $_SESSION['dodajhotel'];
                    unset($_SESSION['dodajhotel']);
                }
            ?>
            
            <h2>Modyfikuj hotel:</h2>
            <form action="modyfikacjaHotelu.php" method="post">
                <p>Wybierz hotel:</p>
                <select name="hotel" required>
                    <?php
                        $dbh = new PDO ('mysql:host=localhost;dbname=BP','root','');
                        $hotel =  "select * from hotel";
                        foreach($dbh -> query($hotel) as $zmienna)
                        {
                            echo '<option value ='.$zmienna['Id'].'>'.$zmienna['Id'].' / '.$zmienna['Nazwa'].' / '.$zmienna['Kraj'].' / '. $zmienna['Miasto'].'</option>';
                        }
                    ?>
                </select><p></p>
                
                <input type="submit" value="Modyfikuj hotel">
                
            </form>
            <?php
                if(isset($_SESSION['modyfikujhotel']))
                {
                    echo $_SESSION['modyfikujhotel'];
                    unset($_SESSION['modyfikujhotel']);
                }
            ?>
            
            <p><a href="index.php">Powrót do strony głównej</a></p>
                </div>
        </article>
        
        <footer>
        
        </footer>
        
        
        
        
        
        
                
    </body>
</HTML>